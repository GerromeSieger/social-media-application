import * as express from "express"
import * as bodyParser from "body-parser"
import connect from './db/conn'
import * as dotenv from "dotenv"
import swaggerDocs from "./docs/swagger"
import userRouter from "./routes/UserRoute"
import postRouter from './routes/PostRoute';
import likeRouter from './routes/LikeRoute';
import commentRouter from './routes/CommentRoute';
import userFollowRouter from './routes/UserFollowRoute';
import notificationRouter from './routes/NotificationRoute';
import { errorMiddleware } from './middleware/errorMiddleware';

    // create express app
    const app = express()
    app.use(bodyParser.json())

    dotenv.config()
    // start express server
    app.use('/api/', userRouter);
    app.use('/api/', postRouter);
    app.use('/api/', likeRouter);
    app.use('/api/', commentRouter);
    app.use('/api/', notificationRouter);
    app.use('/api/', userFollowRouter);

    app.use(errorMiddleware);
    const port = process.env.PORT

    connect().then(() => {
        try {
            app.listen(port, () => {
                console.log(`Server connected to http://localhost:${port}`);
            })
            // Swagger Page
            swaggerDocs(app, 3000)
        } catch (error) {
            console.log('Cannot connect to the server')
        }
    }).catch(error => {
        console.log("Invalid database connection...!");
    })
