export const paths = {
    '/api/user/get': {
        get: {
            tags: ['User Controller'],
            summary: 'Get User',
            responses: {
                '200': {
                    description: 'Created',
                },
                '409': {
                    description: 'Conflict',
                },
                '404': {
                    description: 'Not Found',
                },
                '500': {
                    description: 'Server Error',
                },
            },
        },
    },
    '/api/user/register': {
        post: {
            tags: ['User Controller'],
            summary: 'Sign Up',
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            required: ['username', 'password', 'email', 'fullname', 'phoneNumber',],
                            properties: {
                                username: {
                                    type: 'string',
                                    default: 'gerrome',
                                },
                                password: {
                                    type: 'string',
                                    default: '123sdfwresdf45',
                                },                                
                                email: {
                                    type: 'string',
                                    default: 'vsifah@gmail.com',
                                },
                                fullname: {
                                    type: 'string',
                                    default: 'Victor Ifah',
                                },                                
                                phonenumber: {
                                    type: 'string',
                                    default: '080890901',
                                }
                            },
                        },
                    },
                },
            },
            responses: {
                '201': {
                    description: 'Created',
                },
                '409': {
                    description: 'Conflict',
                },
                '404': {
                    description: 'Not Found',
                },
                '500': {
                    description: 'Server Error',
                },
            },
        },
    },
    '/api/user/login': {
        post: {
            tags: ['User Controller'],
            summary: 'Sign In',
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            required: ['email', 'password'],
                            properties: {
                                email: {
                                    type: 'string',
                                    default: 'vsifah@gmail.com',
                                },
                                password: {
                                    type: 'string',
                                    default: '123sdfwresdf45',
                                },
                            },
                        },
                    },
                },
            },
            responses: {
                '201': {
                    description: 'Created',
                },
                '409': {
                    description: 'Conflict',
                },
                '404': {
                    description: 'Not Found',
                },
                '500': {
                    description: 'Server Error',
                },
            },
        },
    },
    '/api/user/update': {
        put: {
            tags: ['User Controller'],
            summary: 'Update User',
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            required: ['fullName'],
                            properties: {
                                fullName: {
                                    type: 'string',
                                    default: 'Gerrome Ifah',
                                },
                            },
                        },
                    },
                },
            },
            responses: {
                '200': {
                    description: 'Created',
                },
                '409': {
                    description: 'Conflict',
                },
                '404': {
                    description: 'Not Found',
                },
                '500': {
                    description: 'Server Error',
                },
            },
        },
    },
    '/api/posts': {
        post: {
          tags: ['Post Controller'],
          summary: 'Create a Post',
          security: [
            {
              bearerAuth: [],
            },
          ],
          requestBody: {
            required: true,
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  required: ['text'],
                  properties: {
                    text: {
                      type: 'string',
                      default: 'This is a sample post',
                    },
                    image: {
                      type: 'string',
                      default: 'https://example.com/image.jpg',
                    },
                    video: {
                      type: 'string',
                      default: 'https://example.com/video.mp4',
                    },
                  },
                },
              },
            },
          },
          responses: {
            '201': {
              description: 'Created',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/feed': {
        get: {
          tags: ['Post Controller'],
          summary: 'Get Feed Posts',
          security: [
            {
              bearerAuth: [],
            },
          ],
          parameters: [
            {
              in: 'query',
              name: 'page',
              schema: {
                type: 'integer',
                default: 1,
              },
            },
            {
              in: 'query',
              name: 'limit',
              schema: {
                type: 'integer',
                default: 10,
              },
            },
          ],
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/likes': {
        post: {
          tags: ['Like Controller'],
          summary: 'Like a Post',
          security: [
            {
              bearerAuth: [],
            },
          ],
          requestBody: {
            required: true,
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  required: ['postId'],
                  properties: {
                    postId: {
                      type: 'string',
                      default: '6065a1b9b4b1c3456789abcd',
                    },
                  },
                },
              },
            },
          },
          responses: {
            '201': {
              description: 'Created',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
        delete: {
          tags: ['Like Controller'],
          summary: 'Unlike a Post',
          security: [
            {
              bearerAuth: [],
            },
          ],
          requestBody: {
            required: true,
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  required: ['postId'],
                  properties: {
                    postId: {
                      type: 'string',
                      default: '6065a1b9b4b1c3456789abcd',
                    },
                  },
                },
              },
            },
          },
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/posts/{postId}/likes': {
        get: {
          tags: ['Like Controller'],
          summary: 'Get Post Likes',
          parameters: [
            {
              in: 'path',
              name: 'postId',
              required: true,
              schema: {
                type: 'string',
              },
            },
          ],
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/comments': {
        post: {
          tags: ['Comment Controller'],
          summary: 'Create a Comment',
          security: [
            {
              bearerAuth: [],
            },
          ],
          requestBody: {
            required: true,
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  required: ['postId', 'text'],
                  properties: {
                    postId: {
                      type: 'string',
                      default: '6065a1b9b4b1c3456789abcd',
                    },
                    text: {
                      type: 'string',
                      default: 'This is a sample comment',
                    },
                  },
                },
              },
            },
          },
          responses: {
            '201': {
              description: 'Created',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/comments/{commentId}': {
        delete: {
          tags: ['Comment Controller'],
          summary: 'Delete a Comment',
          security: [
            {
              bearerAuth: [],
            },
          ],
          parameters: [
            {
              in: 'path',
              name: 'commentId',
              required: true,
              schema: {
                type: 'string',
              },
            },
          ],
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/posts/{postId}/comments': {
        get: {
          tags: ['Comment Controller'],
          summary: 'Get Post Comments',
          parameters: [
            {
              in: 'path',
              name: 'postId',
              required: true,
              schema: {
                type: 'string',
              },
            },
          ],
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/notifications': {
        post: {
          tags: ['Notification Controller'],
          summary: 'Create a Notification',
          security: [
            {
              bearerAuth: [],
            },
          ],
          requestBody: {
            required: true,
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  required: ['type', 'postId', 'commentId'],
                  properties: {
                    type: {
                      type: 'string',
                      enum: ['mention', 'like', 'comment'],
                      default: 'like',
                    },
                    postId: {
                      type: 'string',
                      default: '6065a1b9b4b1c3456789abcd',
                    },
                    commentId: {
                      type: 'string',
                      default: '6065a1b9b4b1c3456789abce',
                    },
                  },
                },
              },
            },
          },
          responses: {
            '201': {
              description: 'Created',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
        get: {
          tags: ['Notification Controller'],
          summary: 'Get User Notifications',
          security: [
            {
              bearerAuth: [],
            },
          ],
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/notifications/{notificationId}/read': {
        put: {
          tags: ['Notification Controller'],
          summary: 'Mark Notification as Read',
          security: [
            {
              bearerAuth: [],
            },
          ],
          parameters: [
            {
              in: 'path',
              name: 'notificationId',
              required: true,
              schema: {
                type: 'string',
              },
            },
          ],
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/follow': {
        post: {
          tags: ['User Follow Controller'],
          summary: 'Follow a User',
          security: [
            {
              bearerAuth: [],
            },
          ],
          requestBody: {
            required: true,
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  required: ['followUserId'],
                  properties: {
                    followUserId: {
                      type: 'string',
                      default: '6065a1b9b4b1c3456789abcd',
                    },
                  },
                },
              },
            },
          },
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },
      '/api/unfollow': {
        post: {
          tags: ['User Follow Controller'],
          summary: 'Unfollow a User',
          security: [
            {
              bearerAuth: [],
            },
          ],
          requestBody: {
            required: true,
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  required: ['unfollowUserId'],
                  properties: {
                    unfollowUserId: {
                      type: 'string',
                      default: '6065a1b9b4b1c3456789abcd',
                    },
                  },
                },
              },
            },
          },
          responses: {
            '200': {
              description: 'OK',
            },
            '409': {
              description: 'Conflict',
            },
            '404': {
              description: 'Not Found',
            },
            '500': {
              description: 'Server Error',
            },
          },
        },
      },          
}        