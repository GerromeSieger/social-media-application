// src/swaggerDocs.ts
import swaggerUi from 'swagger-ui-express';
import { Express, Request, Response } from 'express';
import { serve, setup } from 'swagger-ui-express';
import { paths } from './path';

const swaggerSpec = {
  openapi: '3.0.0',
  info: {
    title: 'Social Media Api',
    description: 'API endpoints for the backend services',
    version: '1.0.0',
  },
  servers: [
    {
      url: 'http://localhost:3000/',
      description: 'Local server',
    },
  ],
  paths,
  components: {
    securitySchemes: {
      bearerAuth: {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
      },
    },
  },
  security: [
    {
      bearerAuth: [],
    },
  ],  
};

function swaggerDocs(app: Express, port: number) {
  // Swagger Page
  app.use('/docs', serve, setup(swaggerSpec));

  // Documentation in JSON format
  app.get('/docs.json', (req: Request, res: Response) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });
}

export default swaggerDocs;