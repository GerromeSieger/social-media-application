import { Request, Response, NextFunction } from 'express';
import CommentService from '../services/CommentService';
import * as jwt from 'jsonwebtoken';

interface JwtPayload {
  userId: string;
  // Add any other properties you expect in the JWT payload
}

class CommentController {
  private commentService: CommentService;

  constructor(commentService: CommentService) {
    this.commentService = commentService;
  }

  async createComment(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }   
      const { postId, text } = req.body;
      const comment = await this.commentService.createComment(userId, postId, text);
      res.status(201).json(comment);
    } catch (error) {
      next(error);
    }
  }

  async deleteComment(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }   
      const { commentId } = req.params;
      await this.commentService.deleteComment(userId, commentId);
      res.status(200).json({ message: 'Comment deleted' });
    } catch (error) {
      next(error);
    }
  }

  async getPostComments(req: Request, res: Response, next: NextFunction) {
    try {
      const { postId } = req.params;
      const comments = await this.commentService.getPostComments(postId);
      res.status(200).json(comments);
    } catch (error) {
      next(error);
    }
  }
}

export default CommentController;