import { Request, Response, NextFunction } from 'express';
import LikeService from '../services/LikeService';
import * as jwt from 'jsonwebtoken';

interface JwtPayload {
  userId: string;
  // Add any other properties you expect in the JWT payload
}


class LikeController {
  private likeService: LikeService;

  constructor(likeService: LikeService) {
    this.likeService = likeService;
  }

  async likePost(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }      
      const { postId } = req.body;
      const like = await this.likeService.likePost(userId, postId);
      res.status(201).json(like);
    } catch (error) {
      next(error);
    }
  }

  async unlikePost(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }      
      const { postId } = req.body;
      await this.likeService.unlikePost(userId, postId);
      res.status(200).json({ message: 'Post unliked' });
    } catch (error) {
      next(error);
    }
  }

  async getPostLikes(req: Request, res: Response, next: NextFunction) {
    try {
      const { postId } = req.params;
      const likes = await this.likeService.getPostLikes(postId);
      res.status(200).json(likes);
    } catch (error) {
      next(error);
    }
  }
}

export default LikeController;