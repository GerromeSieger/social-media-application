import { Request, Response, NextFunction } from 'express';
import UserFollowService from '../services/UserFollowService';
import * as jwt from 'jsonwebtoken';

interface JwtPayload {
  userId: string;
  // Add any other properties you expect in the JWT payload
}

class UserFollowController {
  private userFollowService: UserFollowService;

  constructor(userFollowService: UserFollowService) {
    this.userFollowService = userFollowService;
  }

  async followUser(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }
      const { followUserId } = req.body;
      const user = await this.userFollowService.followUser(userId, followUserId);
      res.status(200).json(user);
    } catch (error) {
      next(error);
    }
  }

  async unfollowUser(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }
      const { unfollowUserId } = req.body;
      const user = await this.userFollowService.unfollowUser(userId, unfollowUserId);
      res.status(200).json(user);
    } catch (error) {
      next(error);
    }
  }
}

export default UserFollowController;