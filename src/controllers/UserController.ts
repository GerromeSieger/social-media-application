import { Request, Response, NextFunction } from 'express';
import UserService from '../services/UserService';
import * as jwt from 'jsonwebtoken';

interface JwtPayload {
  userId: string;
  // Add any other properties you expect in the JWT payload
}

class UserController {
  private userService: UserService;

  constructor(userService: UserService) {
    this.userService = userService;
  }

  async signup(req: Request, res: Response, next: NextFunction) {
    try {
      const { username, password, email, fullname, phonenumber } = req.body;

      const user = await this.userService.signup(username, password, email, fullname, phonenumber);

      res.status(201).json({ msg: "User Registered Successfully", user });
    } catch (error) {
      next(error); // Pass the error to the error middleware
    }
  }

  async signin(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, password } = req.body;

      const { user, token } = await this.userService.signin(email, password);

      const { password: _, ...responseUser } = user.toObject();

      res.status(200).json({ msg: 'Login Successful...!', user: responseUser, token });
    } catch (error) {
      next(error); // Pass the error to the error middleware
    }
  }
  
  async updateUserInfo(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }
  
      const updateData = req.body;

      const updatedUser = await this.userService.updateUserInfo(userId, updateData);

      res.status(200).json({ msg: 'User information updated successfully', user: updatedUser });
    } catch (error) {
      next(error);
    }
  }
  
  async getUserInfo(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }

      const user = await this.userService.getUserInfo(userId);

      res.status(200).json({ user });
    } catch (error) {
      next(error);
    }
  }
  
//   async uploadAvatar(req: Request, res: Response, next: NextFunction) {
//     try {
//       const file = req.file;

//       if (!file) {
//         throw new HttpException(400, 'No file provided');
//       }

//       const token = req.headers.authorization?.split(' ')[1];
//       const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
//       const userId = decodedToken.userId;

//       const updatedUser = await this.userService.uploadAvatar(userId, file);

//       res.status(200).json({ msg: 'Profile picture uploaded successfully', user: updatedUser });
//     } catch (error) {
//       next(error);
//     }
//   } 
}

export default UserController;