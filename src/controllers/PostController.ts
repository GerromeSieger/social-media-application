import { Request, Response, NextFunction } from 'express';
import PostService from '../services/PostService';
import * as jwt from 'jsonwebtoken';

interface JwtPayload {
  userId: string;
  // Add any other properties you expect in the JWT payload
}

class PostController {
  private postService: PostService;

  constructor(postService: PostService) {
    this.postService = postService;
  }

  async createPost(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }      
      const { text, image, video } = req.body;
      const post = await this.postService.createPost(userId, text, image, video);
      res.status(201).json(post);
    } catch (error) {
      next(error);
    }
  }

  async getFeedPosts(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }      
      const page = parseInt(req.query.page as string) || 1;
      const limit = parseInt(req.query.limit as string) || 10;
      const posts = await this.postService.getFeedPosts(userId, page, limit);
      res.status(200).json(posts);
    } catch (error) {
      next(error);
    }
  }
}

export default PostController;