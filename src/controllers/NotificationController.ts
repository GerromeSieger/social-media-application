import { Request, Response, NextFunction } from 'express';
import NotificationService from '../services/NotificationService';
import * as jwt from 'jsonwebtoken';

interface JwtPayload {
  userId: string;
  // Add any other properties you expect in the JWT payload
}

class NotificationController {
  private notificationService: NotificationService;

  constructor(notificationService: NotificationService) {
    this.notificationService = notificationService;
  }

  async createNotification(req: Request, res: Response, next: NextFunction) {
    try {
      const { type, postId, commentId } = req.body;
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }   
      const notification = await this.notificationService.createNotification(userId, type, postId, commentId);
      res.status(201).json(notification);
    } catch (error) {
      next(error);
    }
  }

  async getUserNotifications(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET as string) as JwtPayload;
      let userId;
  
      // Check if decodedToken is an object before accessing userId
      if (typeof decodedToken === 'object' && decodedToken !== null) {
        userId = decodedToken.userId;
      } else {
        throw new Error('Invalid token');
      }   
      const notifications = await this.notificationService.getUserNotifications(userId);
      res.status(200).json(notifications);
    } catch (error) {
      next(error);
    }
  }

  async markNotificationAsRead(req: Request, res: Response, next: NextFunction) {
    try {
      const { notificationId } = req.params;
      const notification = await this.notificationService.markNotificationAsRead(notificationId);
      res.status(200).json(notification);
    } catch (error) {
      next(error);
    }
  }
}

export default NotificationController;