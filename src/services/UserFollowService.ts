import User, { UserDocument } from '../models/User';

class UserFollowService {
  async followUser(userId: string, followUserId: string): Promise<UserDocument> {
    const user = await User.findById(userId);
    const followUser = await User.findById(followUserId);

    if (!user || !followUser) {
      throw new Error('User or follow user not found');
    }

    if (user.following.includes(followUserId)) {
      throw new Error('You are already following this user');
    }

    user.following.push(followUserId);
    followUser.followers.push(userId);

    await user.save();
    await followUser.save();

    return user;
  }

  async unfollowUser(userId: string, unfollowUserId: string): Promise<UserDocument> {
    const user = await User.findById(userId);
    const unfollowUser = await User.findById(unfollowUserId);

    if (!user || !unfollowUser) {
      throw new Error('User or unfollow user not found');
    }

    user.following = user.following.filter((id) => id.toString() !== unfollowUserId);
    unfollowUser.followers = unfollowUser.followers.filter((id) => id.toString() !== userId);

    await user.save();
    await unfollowUser.save();

    return user;
  }
}

export function createUserFollowService(): UserFollowService {
  return new UserFollowService();
}

export default UserFollowService;