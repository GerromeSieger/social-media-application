import * as bcrypt from 'bcrypt';
import { HttpException } from '../middleware/errorMiddleware';
import * as jwt from 'jsonwebtoken';
import * as Joi from 'joi';
import User, { UserDocument } from '../models/User';

export class UserService {

  // Validation schemas
  public signupSchema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().min(8).required(),
    email: Joi.string().email().required(),
    fullname: Joi.string().required(),
    phonenumber: Joi.string().required(),
  });

  public loginSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });

  async signup(username: string, password: string, email: string, fullname: string, phonenumber: string) {
    // Validate input data
    const { error } = this.signupSchema.validate({ username, password, email, fullname, phonenumber });
    if (error) {
      throw new HttpException(400, error.details[0].message);
    }

    // Check if the user already exists
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw new HttpException(400, 'User already exists');
    }

    // Hash the password
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    // Create a new user
    const newUser = new User({
      email,
      password: hashedPassword,
      fullname,
      phonenumber,
      verified: false,
    });

    // Save the new user to the database
    const savedUser = await newUser.save();
    return savedUser;
  }

  async signin(email: string, password: string) {
    const { error } = this.loginSchema.validate({ email, password });
    if (error) {
      throw new HttpException(400, error.details[0].message);
    }

    const user = await User.findOne({ email });

    if (!user) {
      throw new HttpException(404, 'User not found');
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      throw new HttpException(400, 'Invalid password');
    }

    const token = jwt.sign(
      {
        userId: user.id,
        email: user.email,
      },
      process.env.JWT_SECRET as string,
      { expiresIn: '720h' }
    );

    return { user, token };
  }

  async findUserByIdAndEmail(userId: string, email: string): Promise<UserDocument | null> {
    return User.findOne({ _id: userId, email });
  }

  async updateUserInfo(userId: string, updateData: Partial<UserDocument>) {
    const user = await User.findById(userId);

    if (!user) {
      throw new HttpException(404, 'User not found');
    }

    // Update properties directly
    user.fullname = updateData.fullname || user.fullname;
    user.phonenumber = updateData.phonenumber || user.phonenumber;
    user.email = updateData.email || user.email;
    // Update other properties as needed

    const updatedUser = await user.save();

    return updatedUser;
  }

  async getUserInfo(userId: string) {
    const user = await User.findById(userId);

    if (!user) {
      throw new HttpException(404, 'User not found');
    }

    return user;
  }

}

export function createUserService(): UserService {
  return new UserService();
}

export default UserService;