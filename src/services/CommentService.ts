import Comment, { CommentDocument } from '../models/Comment';
import Post, { PostDocument } from '../models/Post';
import User, { UserDocument } from '../models/User';
import Notification, { NotificationDocument } from '../models/Notification';

class CommentService {
  async createComment(userId: string, postId: string, text: string): Promise<CommentDocument> {
    const comment = new Comment({
      text,
      user: userId,
      post: postId,
    });
    await comment.save();

    // Create a notification for the post owner
    const post = await Post.findById(postId).populate('user');
    if (post && post.user._id.toString() !== userId) {
      await Notification.create({
        user: post.user._id,
        type: 'comment',
        post: postId,
        comment: comment._id,
      });
    }

    return comment;
  }

  async deleteComment(userId: string, commentId: string): Promise<void> {
    const comment = await Comment.findById(commentId);
    if (!comment) {
      throw new Error('Comment not found');
    }
    if (comment.user.toString() !== userId) {
      throw new Error('You are not authorized to delete this comment');
    }
    await Comment.deleteOne({ _id: commentId });
  }

  async getPostComments(postId: string): Promise<CommentDocument[]> {
    return await Comment.find({ post: postId }).populate('user');
  }
}

export function createCommentService(): CommentService {
  return new CommentService();
}

export default CommentService;
