import Post, { PostDocument } from '../models/Post';
import User, { UserDocument } from '../models/User';

class PostService {
  async createPost(userId: string, text: string, image?: string, video?: string): Promise<PostDocument> {
    const post = new Post({
      text,
      image,
      video,
      user: userId,
    });
    return await post.save();
  }

  async getFeedPosts(userId: string, page: number = 1, limit: number = 10): Promise<PostDocument[]> {
    const user = await User.findById(userId).populate('following');
    const following = user?.following.map((f) => f._id);

    const posts = await Post.find({ user: { $in: [...(following || []), userId] } })
      .sort({ createdAt: -1 })
      .skip((page - 1) * limit)
      .limit(limit)
      .populate('user');

    return posts;
  }
}

export function createPostService(): PostService {
  return new PostService();
}
export default PostService;