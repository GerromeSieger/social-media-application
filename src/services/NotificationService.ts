import Notification, { NotificationDocument } from '../models/Notification';

class NotificationService {
  async createNotification(
    userId: string,
    type: 'mention' | 'like' | 'comment',
    postId?: string,
    commentId?: string
  ): Promise<NotificationDocument> {
    const notification = new Notification({
      user: userId,
      type,
      post: postId,
      comment: commentId,
    });
    return await notification.save();
  }

  async getUserNotifications(userId: string): Promise<NotificationDocument[]> {
    return await Notification.find({ user: userId }).sort({ createdAt: -1 }).populate(['post', 'comment']);
  }

  async markNotificationAsRead(notificationId: string): Promise<NotificationDocument | null> {
    return await Notification.findByIdAndUpdate(notificationId, { read: true }, { new: true });
  }
}

export function createNotificationService(): NotificationService {
  return new NotificationService();
}

export default NotificationService;
