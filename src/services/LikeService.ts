import Like, { LikeDocument } from '../models/Like';
import Post, { PostDocument } from '../models/Post';
import User, { UserDocument } from '../models/User';
import Notification, { NotificationDocument } from '../models/Notification';

class LikeService {
  async likePost(userId: string, postId: string): Promise<LikeDocument> {
    const existingLike = await Like.findOne({ user: userId, post: postId });
    if (existingLike) {
      throw new Error('You have already liked this post');
    }

    const like = new Like({
      user: userId,
      post: postId,
    });
    await like.save();

    // Create a notification for the post owner
    const post = await Post.findById(postId).populate('user');
    if (post && post.user._id.toString() !== userId) {
      await Notification.create({
        user: post.user._id,
        type: 'like',
        post: postId,
      });
    }

    return like;
  }

  async unlikePost(userId: string, postId: string): Promise<void> {
    const like = await Like.findOne({ user: userId, post: postId });
    if (!like) {
      throw new Error('You have not liked this post');
    }
    await Like.deleteOne({ _id: like._id });
  }

  async getPostLikes(postId: string): Promise<UserDocument[]> {
    const likes = await Like.find({ post: postId }).populate('user');
    return likes.map((like) => like.user);
  }
}

export function createLikeService(): LikeService {
  return new LikeService();
}

export default LikeService;