import { Router } from 'express';
import UserFollowController from '../controllers/UserFollowController';
import { createUserFollowService } from '../services/UserFollowService';
import { Auth } from '../middleware/authMiddleware';

const userFollowRouter = Router();
const userFollowService = createUserFollowService();
const userFollowController = new UserFollowController(userFollowService);

userFollowRouter.route('/follow').post(Auth, userFollowController.followUser.bind(userFollowController));
userFollowRouter.route('/unfollow').post(Auth, userFollowController.unfollowUser.bind(userFollowController));

export default userFollowRouter;