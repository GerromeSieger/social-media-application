import { Router } from 'express';
import LikeController from '../controllers/LikeController';
import { createLikeService } from '../services/LikeService';
import { Auth } from '../middleware/authMiddleware';

const likeRouter = Router();
const likeService = createLikeService();
const likeController = new LikeController(likeService);

likeRouter.route('/likes').post(Auth, likeController.likePost.bind(likeController));
likeRouter.route('/likes').delete(Auth, likeController.unlikePost.bind(likeController));
likeRouter.route('/posts/:postId/likes').get(likeController.getPostLikes.bind(likeController));

export default likeRouter;