import { Router } from 'express';
import NotificationController from '../controllers/NotificationController';
import { createNotificationService } from '../services/NotificationService';
import { Auth } from '../middleware/authMiddleware';

const notificationRouter = Router();
const notificationService = createNotificationService();
const notificationController = new NotificationController(notificationService);

notificationRouter.route('/notifications').post(Auth, notificationController.createNotification.bind(notificationController));
notificationRouter.route('/notifications').get(Auth, notificationController.getUserNotifications.bind(notificationController));
notificationRouter.route('/notifications/:notificationId/read').put(Auth, notificationController.markNotificationAsRead.bind(notificationController));

export default notificationRouter;