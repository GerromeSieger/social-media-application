import { Router } from 'express';
import UserController from '../controllers/UserController';
import { createUserService } from '../services/UserService';
import { Auth } from '../middleware/authMiddleware';

const userRouter = Router();
const userService = createUserService();
const userController = new UserController(userService);

userRouter.route('/user/register').post(userController.signup.bind(userController));
userRouter.route('/user/login').post(userController.signin.bind(userController));
userRouter.route('/user/update').put(Auth, userController.updateUserInfo.bind(userController));
userRouter.route('/user/get').get(Auth, userController.getUserInfo.bind(userController));

export default userRouter;
