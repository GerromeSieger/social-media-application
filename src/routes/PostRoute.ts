import { Router } from 'express';
import PostController from '../controllers/PostController';
import { createPostService } from '../services/PostService';
import { Auth } from '../middleware/authMiddleware';

const postRouter = Router();
const postService = createPostService();
const postController = new PostController(postService);

postRouter.route('/posts').post(Auth, postController.createPost.bind(postController));
postRouter.route('/feed').get(Auth, postController.getFeedPosts.bind(postController));

export default postRouter;