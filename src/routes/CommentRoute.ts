import { Router } from 'express';
import CommentController from '../controllers/CommentController';
import { createCommentService } from '../services/CommentService';
import { Auth } from '../middleware/authMiddleware';

const commentRouter = Router();
const commentService = createCommentService();
const commentController = new CommentController(commentService);

commentRouter.route('/comments').post(Auth, commentController.createComment.bind(commentController));
commentRouter.route('/comments/:commentId').delete(Auth, commentController.deleteComment.bind(commentController));
commentRouter.route('/posts/:postId/comments').get(commentController.getPostComments.bind(commentController));

export default commentRouter;