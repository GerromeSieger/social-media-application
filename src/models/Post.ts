import mongoose, { Schema, Document } from 'mongoose';
import { UserDocument } from './User';

export interface PostDocument extends Document {
  text: string;
  image?: string;
  video?: string;
  user: UserDocument['_id'];
  createdAt: Date;
  updatedAt: Date;
}

const PostSchema: Schema = new Schema(
  {
    text: { type: String, required: true },
    image: { type: String },
    video: { type: String },
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  },
  { timestamps: true }
);

const Post = mongoose.model<PostDocument>('Post', PostSchema);

export default Post;