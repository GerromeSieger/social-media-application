import mongoose, { Schema, Document } from 'mongoose';

export interface UserDocument extends Document {
    username: string;
    fullname: string;
    email: string;
    password: string;
    phonenumber: string;
    verified: boolean;
    following: UserDocument['_id'][];
    followers: UserDocument['_id'][];                
}

const UserSchema: Schema = new Schema({
    username: { type: String, },
    fullname: { type: String},
    email: { type: String, required: true},
    password: { type: String},
    phonenumber: { type: String},
    verified: { type: Boolean},
    following: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    followers: [{ type: Schema.Types.ObjectId, ref: 'User' }]     
});

const User = mongoose.model<UserDocument>('User', UserSchema);

export default User;