import mongoose, { Schema, Document } from 'mongoose';
import { UserDocument } from './User';
import { PostDocument } from './Post';

export interface CommentDocument extends Document {
  text: string;
  user: UserDocument['_id'];
  post: PostDocument['_id'];
  createdAt: Date;
  updatedAt: Date;
}

const CommentSchema: Schema = new Schema(
  {
    text: { type: String, required: true },
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    post: { type: Schema.Types.ObjectId, ref: 'Post', required: true },
  },
  { timestamps: true }
);

const Comment = mongoose.model<CommentDocument>('Comment', CommentSchema);

export default Comment;