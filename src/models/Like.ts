import mongoose, { Schema, Document } from 'mongoose';
import { UserDocument } from './User';
import { PostDocument } from './Post';

export interface LikeDocument extends Document {
  user: UserDocument['_id'];
  post: PostDocument['_id'];
  createdAt: Date;
  updatedAt: Date;
}

const LikeSchema: Schema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    post: { type: Schema.Types.ObjectId, ref: 'Post', required: true },
  },
  { timestamps: true }
);

const Like = mongoose.model<LikeDocument>('Like', LikeSchema);

export default Like;