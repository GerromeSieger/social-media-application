import mongoose, { Schema, Document } from 'mongoose';
import { UserDocument } from './User';
import { PostDocument } from './Post';
import { CommentDocument } from './Comment';

export interface NotificationDocument extends Document {
  user: UserDocument['_id'];
  type: 'mention' | 'like' | 'comment';
  post?: PostDocument['_id'];
  comment?: CommentDocument['_id'];
  createdAt: Date;
  read: boolean;
}

const NotificationSchema: Schema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    type: { type: String, enum: ['mention', 'like', 'comment'], required: true },
    post: { type: Schema.Types.ObjectId, ref: 'Post' },
    comment: { type: Schema.Types.ObjectId, ref: 'Comment' },
    read: { type: Boolean, default: false },
  },
  { timestamps: true }
);

const Notification = mongoose.model<NotificationDocument>('Notification', NotificationSchema);

export default Notification;