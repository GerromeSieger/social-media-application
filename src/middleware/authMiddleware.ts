import * as jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';

export interface DecodedToken {
  userId: string;
  // Add other properties as needed
}

declare global {
  namespace Express {
    interface Request {
      user?: DecodedToken;
    }
  }
}

/**
 * Auth middleware
 */
export const Auth = async (req: Request, res: Response, next: NextFunction) => {
  try {
    // Access the authorization header to validate the request
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ error: 'Authorization header missing' });
    }

    const token = authHeader.split(' ')[1];

    // Verify the token and retrieve the user details
    const decodedToken = await jwt.verify(token, process.env.JWT_SECRET as string) as DecodedToken;

    // Use a type assertion to tell TypeScript that the 'user' property exists
    (req as Request & { user: DecodedToken }).user = decodedToken;

    next();
  } catch (error) {
    res.status(401).json({ error: 'Authentication Failed!' });
  }
};