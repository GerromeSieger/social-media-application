# Social Media API with Swagger Documentation

This is a Social Media Application built with Nodejs, Expressjs, Typescript and MongoDB that has the following functionality:

User Management:
- Users can register and create accounts.
- Users can log in and authenticate themselves (consider options like JWT or session-based authentication).

Posts and Feed:
- Users can create posts with text and optional image/video attachments.
- Users can follow other users.
- Users can see posts from the people they follow in their personalized feed.
- Pagination for retrieving large amounts of data efficiently.

Likes and Comments:
- Users can like and comment on posts created by others.
- Users can see the number of likes and comments on a post.

Notifications:
- Users can receive notifications for mentions, likes, and comments.

# Additional Info

There is also validation with joi and an error middleware to handle errors.

# To start the application run the following commands:

yarn install

yarn dev

Access the swagger documentation on http://localhost:3000/docs